package com.imooc.ad.dump;

import org.nutz.lang.Files;

import java.io.File;

public class DConstant {

    public static final String USER_HOME_DIR = System.getProperties().getProperty("user.home");
    public static final String DATA_ROOT_DIR = USER_HOME_DIR+ File.separator+"imooc"+File.separator+"mysql_data"+File.separator;

    // 各个表数据的存储文件名
    public static final String AD_PLAN ="ad_plan.data";
    public static final String AD_UNIT ="ad_unit.data";
    public static final String AD_CREATIVE ="ad_creative.data";
    public static final String AD_CREATIVE_UNIT ="ad_creative_unit.data";
    public static final String AD_UNIT_IT ="ad_unit_it.data";
    public static final String AD_UNIT_DISTRICT ="ad_unit_district.data";
    public static final String AD_UNIT_KEYWORD ="ad_unit_keyword.data";


    static {
        Files.createDirIfNoExists(DATA_ROOT_DIR);

    }




}
