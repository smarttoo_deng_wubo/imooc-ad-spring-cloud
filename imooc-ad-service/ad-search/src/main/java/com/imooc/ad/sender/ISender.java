package com.imooc.ad.sender;

import com.imooc.ad.mysql.dto.MySqlRowData;

/**
 * Created by deng_wb on 2019/4/13
 */
public interface ISender {

    void sender(MySqlRowData rowData);
}
