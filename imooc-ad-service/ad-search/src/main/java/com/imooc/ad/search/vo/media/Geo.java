package com.imooc.ad.search.vo.media;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class Geo {
    // 精度
    private Float latitude;
    // 维度
    private Float longitude;
    // 城市
    private String city;
    // 省
    private String province;
}
