package com.imooc.ad.service;

import com.github.shyiko.mysql.binlog.BinaryLogClient;
import com.github.shyiko.mysql.binlog.event.*;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BinlogServiceTest {

    /**
     * UpdateRowsEventData{tableId=99, includedColumnsBeforeUpdate={0, 1, 2, 3, 4, 5, 6, 7}, includedColumns={0, 1, 2, 3, 4, 5, 6, 7}, rows=[
     *     {before=[12, 10, 第二个推广单元, 1, 1, 15000000, Mon Jan 01 08:00:00 CST 2018, Mon Jan 01 08:00:00 CST 2018], after=[12, 10, 第二个推广单元, 1, 1, 15000000, Mon Jan 01 08:00:00 CST 2018, Thu Feb 01 08:00:00 CST 2018]}
     * ]}
     *
     *  Mon Jan 01 08:00:00 CST 2018
     * @param args
     * @throws Exception
     */

    public static void main(String[] args) throws Exception {
        BinaryLogClient client = new BinaryLogClient(
                "127.0.0.1",
                3306,
                "root",
                "root"
        );
      //  client.setBinlogFilename("");
     //   client.setBinlogPosition();

        client.registerEventListener(event -> {
            EventData data = event.getData();
            if (data instanceof UpdateRowsEventData){
                log.debug("Update-----------------");
                log.debug(data.toString());

            }else if (data instanceof WriteRowsEventData){
                log.debug("Write-----------------");
                log.debug(data.toString());
            }else if (data instanceof DeleteRowsEventData){
                log.debug("Delete-----------------");
                log.debug(data.toString());
            }else if (data instanceof TableMapEventData){
                log.debug("TableMap-----------------");
                log.debug(data.toString());
            }
        });

        client.connect();

    }
}
