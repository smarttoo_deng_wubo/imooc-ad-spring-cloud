package com.imooc.ad.search;

import com.imooc.ad.search.vo.SearchRequest;
import com.imooc.ad.search.vo.SearchResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


public interface ISearch {

    SearchResponse fetchAds(SearchRequest request);


}
