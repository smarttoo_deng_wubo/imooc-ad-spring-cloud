package com.imooc.ad.index.keyword;

import com.imooc.ad.index.IndexAware;
import com.imooc.ad.utils.CommonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

@Slf4j
@Component
public class UnitKeywordIndex implements IndexAware<String,Set<Long>> {
    /** 关键词与广告单元 一对多关联关系 (反向索引)*/
    private static Map<String,Set<Long>> keywordUnitMap;
    /** 广告单元与关键词 一对多关联关系 (正向索引)*/
    private static Map<Long,Set<String>> unitKeywordMap;

    static{
        keywordUnitMap = new ConcurrentHashMap<>();
        unitKeywordMap = new ConcurrentHashMap<>();
    }

    /** 根据关键词获取广告单元ID集合 */
    @Override
    public Set<Long> get(String key) {

        if (StringUtils.isEmpty(key)){
            return Collections.emptySet();
        }
        Set<Long> result = keywordUnitMap.get(key);
        if (result==null){
            return Collections.emptySet();
        }
        return result;
    }

    /** 设置关键词对应的广告单元ID集合 */
    @Override
    public void add(String key, Set<Long> value) {
        log.info("UnitKeywordIndex, before add: {}", unitKeywordMap);
        // 返回一个绑定了key值的set集合
        Set<Long> unitIdSet = CommonUtils.getorCreate(key,keywordUnitMap,ConcurrentSkipListSet::new);
        unitIdSet.addAll(value);
        value.forEach(unitId->{
            Set<String> keywordSet = CommonUtils.getorCreate(unitId, unitKeywordMap, ConcurrentSkipListSet::new);
            keywordSet.add(key);

        });

        log.info("UnitKeywordIndex, after add: {}", unitKeywordMap);
    }

    @Override
    public void update(String key, Set<Long> value) {
        log.error("keyword index can not support update");
    }

    @Override
    public void delete(String key, Set<Long> value) {
        log.info("UnitKeywordIndex, before delete: {}", unitKeywordMap);
        Set<Long> unitIds = CommonUtils.getorCreate(key,keywordUnitMap,ConcurrentSkipListSet::new);
        unitIds.removeAll(value);
        value.forEach(unitId->{
            Set<String> keywordSet = CommonUtils.getorCreate(unitId, unitKeywordMap, ConcurrentSkipListSet::new);
            keywordSet.remove(key);
        });


        log.info("UnitKeywordIndex, after delete: {}", unitKeywordMap);
    }

    public boolean match(Long unitId, List<String> keywords){
        if (unitKeywordMap.containsKey(unitId)&&CollectionUtils.isNotEmpty(unitKeywordMap.get(unitId))){

            Set<String> unitKeywords = unitKeywordMap.get(unitId);
            // 判断 keywords 是否为 unitKeywords 的子集
            return CollectionUtils.isSubCollection(keywords,unitKeywords);
        }
        return false;
    }
}
