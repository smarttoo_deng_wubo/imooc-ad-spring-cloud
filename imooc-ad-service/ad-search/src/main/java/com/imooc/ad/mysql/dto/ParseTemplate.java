package com.imooc.ad.mysql.dto;

import com.imooc.ad.mysql.constant.OpType;
import lombok.Data;

import java.util.*;
import java.util.function.Supplier;

/**
 *   json 文件的最终解析
 */

@Data
public class ParseTemplate {

    // 声明数据库名称
    private String database;
    // 声明 表名对应 表模板的键值对
    private Map<String, TableTemplate> tableTemplateMap = new HashMap<>();

    // 解析json模板
    public static ParseTemplate parse(Template _template) {

        ParseTemplate template = new ParseTemplate();
        template.setDatabase(_template.getDatabase());

        for (JsonTable table : _template.getTables()) {
            String name = table.getTableName();
            Integer level = table.getLevel();

            TableTemplate tableTemplate = new TableTemplate();
            tableTemplate.setTableName(name);
            tableTemplate.setLevel(level.toString());
            template.tableTemplateMap.put(name, tableTemplate);
            // 遍历操作类型对应的列
            Map<OpType, List<String>> opTypeFieldSetMap = tableTemplate.getOpTypeFieldSetMap();

            for (JsonTable.Column column : table.getInsert()) {
                getAndCreateIfNeed(OpType.ADD, opTypeFieldSetMap, ArrayList::new).add(column.getColumn());
            }

            for (JsonTable.Column column : table.getUpdate()) {
                getAndCreateIfNeed(OpType.UPDATE, opTypeFieldSetMap, ArrayList::new).add(column.getColumn());
            }

            for (JsonTable.Column column : table.getDelete()) {
                getAndCreateIfNeed(OpType.DELETE, opTypeFieldSetMap, ArrayList::new).add(column.getColumn());
            }

        }
        return template;

    }

    private static <T, R> R getAndCreateIfNeed(T key, Map<T, R> map, Supplier<R> factory) {

        // factory.get() 获取到的是空的ArrayList 集合
        return map.computeIfAbsent(key, k -> factory.get());
    }


    public static void main(String[] args) {


        Map<OpType, List<String>> opTypeFieldSetMap = new HashMap<>();

        getAndCreateIfNeed(OpType.DELETE, opTypeFieldSetMap, ArrayList::new).add("test");
        getAndCreateIfNeed(OpType.UPDATE, opTypeFieldSetMap, ArrayList::new).add("test1");
        getAndCreateIfNeed(OpType.UPDATE, opTypeFieldSetMap, ArrayList::new).add("test2");

        System.out.println(opTypeFieldSetMap);


    }

}
